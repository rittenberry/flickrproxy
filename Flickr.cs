namespace flickrapi
{
    public class FlickrSettings
    {
        public string ApiKey { get; set; }
        public string UserId { get; set; }
        public string ApiUrl { get; set; }
    }
}