using Microsoft.AspNetCore.Builder;

namespace flickrapi
{
    public static class FlickerApiProxyExtensions
    {
        public static IApplicationBuilder UseFlickerApiProxy(this IApplicationBuilder builder)
        {

            return builder.UseMiddleware<FlickrApiProxyMiddleware>();
        }
    }
}