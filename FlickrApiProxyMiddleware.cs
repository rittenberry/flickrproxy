

using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace flickrapi
{
    public class FlickrApiProxyMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly FlickrSettings _appSettings;
        private readonly ILogger _logger;
        private readonly HttpClient _httpClient;
        public FlickrApiProxyMiddleware(RequestDelegate next, IOptions<FlickrSettings> appSettings, HttpClient httpClient, ILogger<FlickrApiProxyMiddleware> logger)
        {
            _next = next;
            _appSettings = appSettings.Value;
            _httpClient = httpClient;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            var path = context.Request.Path;
            var queryString = context.Request.QueryString.Value;
            _logger.LogDebug("url=" +_appSettings.ApiUrl);
            const string apiEndpoint = "/api/flickr/";
            if ( !path.Value.StartsWith( apiEndpoint ) )
                //continues through the rest of the pipeline
                await _next.Invoke(context);
            else
            {
                var response = await _httpClient.GetAsync( $"{_appSettings.ApiUrl}{queryString}&api_key={_appSettings.ApiKey}&user_id={_appSettings.UserId}" );
                var result = await response.Content.ReadAsStringAsync();

                context.Response.StatusCode = (int)response.StatusCode;
                await context.Response.WriteAsync( result );
            }
        }    
    }
}